# alt:V TypeScript + Rollup
## How to use it
First usage: <br>
`npm i` <br>

Auto Compiler: <br>
`npm run watch` 

Build for Production: <br>
`npm run build`

## Editing .env

Rename `.env.example` => `.env` <br>
Edit `BUILD_DIR =` to your preference.

## Adding Resources
- Creating a new Folder for the Resource <br>
- Add a package.json with the following format:
<br>

<pre><code>{
   "name": "yourgamemode",
   "isGameResource": true,
   "server": {
     "convertedModules": [],
     "externals": []
   },
   "client": {
     "externals": []
   }
 }</code></pre>