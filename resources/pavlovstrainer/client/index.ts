import * as alt from 'alt-client';

// @ts-ignore
alt.log(alt.File.read("@pavlovstrainer_assets/data/addon_vehicles.json"));

alt.log('PAVLOVSTRAINER Client started');

alt.loadRmlFont("./rml/fonts/Roboto-Bold.ttf", "RobotoBold", false, true);
alt.loadRmlFont("./rml/fonts/Raleway-Bold.ttf", "RalewayBold", false, true);
alt.loadRmlFont("./rml/fonts/Inter-Bold.ttf", "InterBold", false, true);
alt.toggleRmlControls(false);

const document: alt.RmlDocument = new alt.RmlDocument("/rml/view.rml");
document.show(false, true);